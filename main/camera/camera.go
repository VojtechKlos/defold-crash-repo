components {
  id: "script"
  component: "/main/camera/camera.script"
  position {
    x: 0.0
    y: 0.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
  properties {
    id: "useViewArea"
    value: "true"
    type: PROPERTY_TYPE_BOOLEAN
  }
  properties {
    id: "viewArea"
    value: "5.0, 5.0, 0.0"
    type: PROPERTY_TYPE_VECTOR3
  }
  properties {
    id: "expandView"
    value: "false"
    type: PROPERTY_TYPE_BOOLEAN
  }
  properties {
    id: "fixedArea"
    value: "true"
    type: PROPERTY_TYPE_BOOLEAN
  }
  properties {
    id: "fixedWidth"
    value: "false"
    type: PROPERTY_TYPE_BOOLEAN
  }
  properties {
    id: "fixedHeight"
    value: "false"
    type: PROPERTY_TYPE_BOOLEAN
  }
}
