-- GLOBAL CONFIG --
local G = {}

-- Default values --
G.defaults = {}
G.defaults.is_game_paused = true

-- Player
G.defaults.player = {}

G.defaults.player.max_health = 20
G.defaults.player.health = 20
G.defaults.player.max_shield = 20
G.defaults.player.shield = 20
G.defaults.player.score = 0
G.defaults.player.fire_rate_multiplier = 1
G.defaults.player.projectile_size_multiplier = 1
G.defaults.player.speed_multiplier = 1
G.defaults.player.is_alive = true
G.defaults.player.powerups = {}
G.defaults.player.gun_num = 1

-- Powerups
G.defaults.powerups = {}
G.defaults.powerups.available = {"laser_beam", "dual_cannon"}

-- Player stats --
G.player = {}

-- Weapons !!! --
G.weapons = {}

-- Index based lookup table
G.weapons.lut = {"default", "laser_beam"}

-- Default
G.weapons.default = {}
G.weapons.default.speed = 700
G.weapons.default.interval = 0.7
G.weapons.default.size = 0.1

-- Laser Beam
G.weapons.laser_beam = {}
G.weapons.laser_beam.speed = 3000
G.weapons.laser_beam.interval = 1.3
G.weapons.laser_beam.size = 1
G.weapons.laser_beam.penetrate_chance = 20


-- Background Animations --
G.backgrounds = {}
G.backgrounds.is_starting_background = true
G.backgrounds.is_moving = true

-- Powerups --
G.powerups = {}

return G